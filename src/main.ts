import { NestFactory } from '@nestjs/core'
import { json } from 'body-parser'
import { AppModule } from './main/app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bodyParser: false })
  app.enableCors()
  app.use(json({ limit: '5mb' }))
  await app.listen(3000)
}
bootstrap()
